import { loanFormSlice, updateFormState } from './loanFormSlice'
import { ILoanFormValues, initialValues } from '../../components/organism/forms/LoanForm/schema'
const { reducer } = loanFormSlice
describe('counter reducer', () => {
	const initialState: ILoanFormValues = initialValues
	it('should handle initial state', () => {
		expect(reducer(undefined, { type: 'unknown' })).toEqual(initialState)
	})

	it('should handle update form state', () => {
		const actual = reducer(
			initialState,
			updateFormState({
				loan: 25000,
				insurance: true,
				term: 24
			})
		)
		expect(actual.loan).toEqual(25000)
		expect(actual.insurance).toEqual(true)
		expect(actual.term).toEqual(24)
	})
})
