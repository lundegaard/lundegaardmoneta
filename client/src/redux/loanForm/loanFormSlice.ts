import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store'
import { ILoanFormValues, initialValues } from '../../components/organism/forms/LoanForm/schema'

export const LAON_FORM_SLICE = 'LAON_FORM_SLICE'
export const loanFormSlice = createSlice({
	name: LAON_FORM_SLICE,
	initialState: initialValues,
	reducers: {
		updateFormState: (state: ILoanFormValues, action: PayloadAction<ILoanFormValues>) => {
			return action.payload
		}
	}
})

export const { updateFormState } = loanFormSlice.actions

export const getFormState = (state: RootState): ILoanFormValues => state.loanForm
