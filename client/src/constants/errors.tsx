export const ERRORS = {
	REQUIRED_ERROR: 'Field is required',
	MUST_BE_NUMBER: 'Value of field has to be number',
	WRONG_POLICY_NUMBER: 'Wrong policy number format',
	MIN_5000: 'Min value is 5 000',
	MAX_1500000: 'Max value is 1 500 000',
	MIN_24: 'Min term is 2 years',
	MAX_120: 'Max term is 10 years'
}
