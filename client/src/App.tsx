import React from 'react'
import './App.css'
import { LoanPage } from './pages/LoanPage'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { LookupPage } from './pages/Lookup'
import { PATHS } from './constants/PATHS'

const App: React.FC = () => {
	return (
		<div className='App'>
			<div className={'App-header'}>
				<BrowserRouter>
					<Routes>
						<Route path={PATHS.HOME} element={<LoanPage />} />
						<Route path={PATHS.LOOKUP} element={<LookupPage />} />
					</Routes>
				</BrowserRouter>
			</div>
		</div>
	)
}

export default App
