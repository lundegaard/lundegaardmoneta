import { Box, Card } from '@mui/material'
import React from 'react'
import { usePostLoanMutation } from '../../../hooks/query/usePostLoanComputeMutation'
import ErrorBot from '../../../assets/images/errorBot.png'
import ClipLoader from 'react-spinners/ClipLoader'
import { COLOR_PRIMARY } from '../../../utils/theme'
import { LoanResultTable } from './LoanResultTable'

export const LoanFormResultCard: React.FC = () => {
	const [, { isLoading, isError, data }] = usePostLoanMutation()
	return (
		<Card sx={{ minHeight: 300, width: '100%', minWidth: 400 }} variant={'outlined'}>
			<Box display={'flex'} alignItems={'center'} alignContent={'center'} minHeight={300} justifyContent={'center'}>
				{isLoading && <ClipLoader color={COLOR_PRIMARY} />}
				{!isLoading && isError && <img width={240} src={ErrorBot} height={250} alt={'error'} />}
				{!isLoading && !isError && data && <LoanResultTable data={data} />}
			</Box>
		</Card>
	)
}
