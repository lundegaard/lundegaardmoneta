import { CardContent, Typography } from '@mui/material'
import { CardResultRow } from '../../atoms/CardResultRow'
import { formatNumber } from '../../../utils/formatNumber'
import React from 'react'
import { ILoanComputeResponse } from '../../../api/loan/types'

export interface LoanResultTableProps {
	data: ILoanComputeResponse
}

export const LoanResultTable: React.FC<LoanResultTableProps> = ({ data }) => {
	return (
		<CardContent sx={{ width: '100%' }}>
			<Typography mb={3} variant={'h5'}>
				Vaše výhodná půjčka
			</Typography>
			<CardResultRow label={'Měsíčně'} valueVariant={'h5'} value={`${formatNumber(data.monthly)} Kč`} />
			<CardResultRow label={'Roční úroková sazba od'} value={`${data.rate} %`} />
			<CardResultRow label={'RPSN'} value={'7,01 %'} />
			{data.insurance !== 0 && <CardResultRow label={'Pojištění'} value={`${formatNumber(data.insurance)} Kč`} />}
			<CardResultRow label={'Poplatek za sjednání na pobočce'} value={`0 Kč`} />
			<CardResultRow label={'Za dobu trvání zaplatíte'} value={`${formatNumber(data.overallAmount)} Kč`} />
		</CardContent>
	)
}
