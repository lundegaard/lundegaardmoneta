import { Box, Typography } from '@mui/material'
import React from 'react'

export interface FormRowWithTextProps {
	label: string
	content: string | number | React.ReactNode
}
export const FormRowWithText: React.FC<FormRowWithTextProps> = ({ label, content }) => {
	return (
		<Box mt={1} display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
			<Typography variant={'body1'}>{label}</Typography>
			<Box display={'flex'} alignItems={'center'}>
				{typeof content === 'string' || typeof content === 'number' ? (
					<Typography variant={'body1'} fontWeight={700}>
						{content}
					</Typography>
				) : (
					content
				)}
			</Box>
		</Box>
	)
}
