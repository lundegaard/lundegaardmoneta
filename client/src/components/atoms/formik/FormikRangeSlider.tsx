import { useField } from 'formik'
import React from 'react'
import { Slider, SliderProps } from '@mui/material'

export interface FormikFieldProps {
	fieldName: string
}

export type FormikRangeSliderProps = Omit<SliderProps, 'onChange' | 'onBlur' | 'onFocus' | 'value' | 'error'>
export const FormikRangeSlider: React.FC<FormikFieldProps & FormikRangeSliderProps> = ({ fieldName, ...props }) => {
	const [field] = useField(fieldName)
	return <Slider {...field} {...props} />
}
