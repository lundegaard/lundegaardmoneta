import { useField } from 'formik'
import React from 'react'
import { InputBaseComponentProps, TextField } from '@mui/material'
import { TextFieldProps } from '@mui/material/TextField/TextField'
import { useFieldHasError } from '../../../hooks/useFieldHasError'
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'

const defaultMaskOptions = {
	prefix: '',
	suffix: '',
	includeThousandsSeparator: true,
	thousandsSeparatorSymbol: ' ',
	allowDecimal: true,
	decimalSymbol: '.',
	decimalLimit: 2, // how many digits allowed after the decimal
	integerLimit: 7, // limit length of integer numbers
	allowNegative: false,
	allowLeadingZeroes: false,
	guide: false,
	modelClean: true
}
// eslint-disable-next-line no-unused-vars
const TextMaskCustom = (props: InputBaseComponentProps) => {
	const { inputRef, ...other } = props
	const currencyMask = createNumberMask({
		...defaultMaskOptions
	})
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-ignore
	return <MaskedInput {...other} ref={inputRef} guide={false} mask={currencyMask} placeholderChar={'\u2000'} showMask />
}
export interface FormikFieldProps {
	fieldName: string
}

export type FormikTextFieldProps = Omit<TextFieldProps, 'onChange' | 'onBlur' | 'onFocus' | 'value' | 'error'>
export const FormikTextField: React.FC<FormikFieldProps & FormikTextFieldProps> = ({
	fieldName,
	helperText,
	...props
}) => {
	const [field, , helpers] = useField(fieldName)
	const [fieldHasError, errorText] = useFieldHasError(fieldName)
	return (
		<TextField
			{...field}
			{...props}
			onChange={(e) => {
				helpers.setValue(e.target.value.replace(/\D+/g, ''))
			}}
			size={'small'}
			error={fieldHasError}
			helperText={fieldHasError ? errorText : helperText ?? ''}
			InputProps={{
				inputComponent: TextMaskCustom,
				...props.InputProps
			}}
		/>
	)
}
