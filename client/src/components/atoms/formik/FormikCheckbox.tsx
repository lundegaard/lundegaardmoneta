import React from 'react'
import { useField } from 'formik'
import { useFieldHasError } from '../../../hooks/useFieldHasError'
import { FormikFieldProps } from './FormikTextField'
import { Checkbox, FormControl, FormControlLabel, FormGroup, FormHelperText, Typography } from '@mui/material'
import { CheckboxProps } from '@mui/material/Checkbox/Checkbox'

export interface FormikCheckboxProps {
	label: string
	checkboxProps?: CheckboxProps
}
export const FormikCheckbox: React.FC<FormikFieldProps & FormikCheckboxProps> = ({ fieldName, checkboxProps, label }) => {
	const [field] = useField({
		name: fieldName,
		type: 'checkbox'
	})
	const [fieldHasError, errorText] = useFieldHasError(field.name)

	return (
		<FormControl error={fieldHasError}>
			<FormGroup>
				<FormControlLabel
					control={<Checkbox {...field} {...checkboxProps} />}
					label={<Typography variant={'body1'}>{label}</Typography>}
				/>
				{fieldHasError && <FormHelperText>{errorText}</FormHelperText>}
			</FormGroup>
		</FormControl>
	)
}
