import { FormikCheckbox } from '../FormikCheckbox'
import { render } from './utils'
import React from 'react'
import { fireEvent } from '@testing-library/react'

test('renders', async () => {
	const onSubmit = jest.fn()
	const { asFragment } = render(<FormikCheckbox fieldName={'checked'} label={'Zaskrtnout?'} />, {
		initialValues: {
			checked: false
		},
		onSubmit
	})

	expect(asFragment()).toMatchSnapshot()
})

test('checked', async () => {
	// given
	const onSubmit = jest.fn()
	const { getByTestId, findByText } = render(
		<FormikCheckbox
			fieldName={'checked'}
			label={'Zasrtnuto'}
			checkboxProps={{
				inputProps: {
					// eslint-disable-next-line @typescript-eslint/ban-ts-comment
					// @ts-ignore
					'data-testid': 'checked'
				}
			}}
		/>,
		{
			onSubmit,
			initialValues: {
				checked: false
			}
		}
	)

	const input = getByTestId('checked')
	const submit = getByTestId('submit')

	// when
	fireEvent.click(input)
	fireEvent.click(submit)

	await findByText('submitted')

	// then
	expect(onSubmit).toBeCalledWith(
		{
			checked: true
		},
		expect.anything()
	)
})
