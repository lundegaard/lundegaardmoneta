import { render } from './utils'
import { fireEvent } from '@testing-library/react'
import React from 'react'
import { FormikTextField } from '../FormikTextField'

test('renders', async () => {
	const onSubmit = jest.fn()
	const { asFragment } = render(<FormikTextField fieldName={'loan'} label={'Zaskrtnout?'} />, {
		initialValues: {
			loan: '1000'
		},
		onSubmit
	})

	expect(asFragment()).toMatchSnapshot()
})

test('checked', async () => {
	// given
	const onSubmit = jest.fn()
	const { getByTestId, findByText } = render(
		<FormikTextField
			fieldName={'loan'}
			inputProps={{
				'data-testid': 'loan'
			}}
			label={'Zasrtnuto'}
		/>,
		{
			onSubmit,
			initialValues: {
				loan: '0'
			}
		}
	)

	const input = getByTestId('loan')
	const submit = getByTestId('submit')
	// when
	fireEvent.change(input, { target: { value: '1000' } })
	fireEvent.click(submit)

	await findByText('submitted')

	// then
	expect(onSubmit).toBeCalledWith(
		{
			loan: '1000'
		},
		expect.anything()
	)
})
