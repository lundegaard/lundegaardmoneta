import React from 'react'
import { Box, Divider, Typography, TypographyVariant } from '@mui/material'

export interface CardResultRowProps {
	label: string
	value: string
	valueVariant?: TypographyVariant
}
export const CardResultRow: React.FC<CardResultRowProps> = ({ label, value, valueVariant = 'body1' }) => (
	<>
		<Box display={'flex'} my={1} alignItems={'center'} justifyContent={'space-between'}>
			<Typography variant={'body2'}>{label}</Typography>
			<Typography variant={valueVariant} fontWeight={600}>
				{value}
			</Typography>
		</Box>
		<Divider />
	</>
)
