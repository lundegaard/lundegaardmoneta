import { cFieldNames, validationSchema } from './schema'

describe('Testing validation of form', () => {
	it('should not accept numbers of loan', async () => {
		await expect(validationSchema.validateAt(cFieldNames.loan, { loan: 80000000 })).rejects.toBeTruthy()
		await expect(validationSchema.validateAt(cFieldNames.loan, { loan: 4500 })).rejects.toBeTruthy()
		await expect(validationSchema.validateAt(cFieldNames.loan, { loan: 5001 })).resolves.toBeTruthy()
		await expect(validationSchema.validateAt(cFieldNames.loan, { loan: 1500000 })).resolves.toBeTruthy()
	})

	it('should not accept numbers of months', async () => {
		await expect(validationSchema.validateAt(cFieldNames.term, { term: 140 })).rejects.toBeTruthy()
		await expect(validationSchema.validateAt(cFieldNames.term, { term: 12 })).rejects.toBeTruthy()
		await expect(validationSchema.validateAt(cFieldNames.term, { term: 24 })).resolves.toBeTruthy()
		await expect(validationSchema.validateAt(cFieldNames.term, { term: 120 })).resolves.toBeTruthy()
	})
})
