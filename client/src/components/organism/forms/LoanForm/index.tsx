import React from 'react'
import { initialValues, validationSchema } from './schema'
import { LoanFormFormik } from './LoanFormFormik'
import { Form, Formik } from 'formik'
import { useAppDispatch } from '../../../../redux/store'
import { updateFormState } from '../../../../redux/loanForm/loanFormSlice'
import { useNavigate } from 'react-router-dom'
import { PATHS } from '../../../../constants/PATHS'

export const LoanForm: React.FC = () => {
	const dispatch = useAppDispatch()
	const navigate = useNavigate()
	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={(values) => {
				dispatch(updateFormState(values))
				navigate(PATHS.LOOKUP)
			}}
		>
			<Form>
				<LoanFormFormik />
			</Form>
		</Formik>
	)
}
