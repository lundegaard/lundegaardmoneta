import * as yup from 'yup'
import { ERRORS } from '../../../../constants/errors'
import { Typography } from '@mui/material'
import React from 'react'
import { formatNumber } from '../../../../utils/formatNumber'

export interface ILoanFormValues {
	loan: number
	term: number
	insurance: boolean
}

export const initialValues: ILoanFormValues = {
	loan: 750000,
	term: 36,
	insurance: false
}

// eslint-disable-next-line no-unused-vars
export const cFieldNames: { [x in keyof ILoanFormValues]: string } = {
	loan: 'loan',
	term: 'term',
	insurance: 'insurance'
}

export const AMOUNT_MAX = 1500000
export const AMOUNT_MIN = 5000

export const TERM_MONTH_MIN = 24
export const TERM_MONTH_MAX = 120

export const validationSchema: yup.SchemaOf<ILoanFormValues> = yup.object().shape({
	loan: yup
		.number()
		.required(ERRORS.REQUIRED_ERROR)
		.min(AMOUNT_MIN, ERRORS.MIN_5000)
		.max(AMOUNT_MAX, ERRORS.MAX_1500000),
	term: yup
		.number()
		.required(ERRORS.REQUIRED_ERROR)
		.min(TERM_MONTH_MIN, ERRORS.MIN_24)
		.max(TERM_MONTH_MAX, ERRORS.MAX_120),
	insurance: yup.boolean().required(ERRORS.REQUIRED_ERROR)
})

export const rangeTermsSlidersMarks = [
	{
		value: TERM_MONTH_MIN,
		label: (
			<Typography ml={4} variant={'caption'}>
				{TERM_MONTH_MIN} roky
			</Typography>
		)
	},
	{
		value: TERM_MONTH_MAX,
		label: (
			<Typography mr={6} variant={'caption'}>
				{TERM_MONTH_MAX} let
			</Typography>
		)
	}
]

export const rangeAmountSlidersMarks = [
	{
		value: AMOUNT_MIN,
		label: (
			<Typography ml={4} variant={'caption'}>
				{formatNumber(AMOUNT_MIN)} kč
			</Typography>
		)
	},
	{
		value: AMOUNT_MAX,
		label: (
			<Typography mr={6} variant={'caption'}>
				{formatNumber(AMOUNT_MAX)} kč
			</Typography>
		)
	}
]
