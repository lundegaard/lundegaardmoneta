import {
	AMOUNT_MAX,
	AMOUNT_MIN,
	cFieldNames,
	ILoanFormValues,
	rangeAmountSlidersMarks,
	rangeTermsSlidersMarks,
	TERM_MONTH_MAX,
	TERM_MONTH_MIN
} from './schema'
import { useFormikContext } from 'formik'
import React from 'react'
import { Box, Button, Typography } from '@mui/material'
import { FormikTextField } from '../../../atoms/formik/FormikTextField'
import { FormikRangeSlider } from '../../../atoms/formik/FormikRangeSlider'
import { useComputeLoan } from '../../../../hooks/useComputeLoan'
import { parseYearsAndMonthsByMonths } from '../../../../utils/dates'
import { FormikCheckbox } from '../../../atoms/formik/FormikCheckbox'
import { FormRowWithText } from '../../../atoms/FormRowWithText'
import { isEmpty } from 'ramda'

export const LoanFormFormik: React.FC = () => {
	const { values, errors } = useFormikContext<ILoanFormValues>()
	useComputeLoan()
	return (
		<>
			<FormRowWithText
				label={'Kolik byste si u nás rádi půjčili?'}
				content={
					<>
						<FormikTextField
							id={'loan-textField'}
							fieldName={cFieldNames.loan}
							InputProps={{
								sx: {
									'& input': {
										textAlign: 'center',
										fontWeight: 'bold',
										width: 120
									}
								}
							}}
						/>
						<Typography variant={'body2'} ml={1} fontWeight={500}>
							Kč
						</Typography>
					</>
				}
			/>
			<Box mt={2}>
				<FormikRangeSlider
					id={'amount-slider'}
					step={5000}
					min={AMOUNT_MIN}
					marks={rangeAmountSlidersMarks}
					max={AMOUNT_MAX}
					fieldName={cFieldNames.loan}
				/>
			</Box>
			<FormRowWithText
				label={'Délkou splácení si určete výšku splátky'}
				content={parseYearsAndMonthsByMonths(values.term)}
			/>
			<Box mt={2}>
				<FormikRangeSlider
					step={1}
					min={TERM_MONTH_MIN}
					marks={rangeTermsSlidersMarks}
					max={TERM_MONTH_MAX}
					fieldName={cFieldNames.term}
				/>
			</Box>
			<Box mt={2} display={'flex'} justifyContent={'flex-start'}>
				<FormikCheckbox fieldName={cFieldNames.insurance} label={'Přiobjednat pojištění ?'} />
			</Box>
			<Box mt={2} display={'flex'} justifyContent={'flex-start'}>
				<Button type={'submit'} disabled={!isEmpty(errors)} variant={'contained'}>
					Pokračovat
				</Button>
			</Box>
		</>
	)
}
