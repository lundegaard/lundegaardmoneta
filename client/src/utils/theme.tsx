import { createTheme } from '@mui/material/styles'

export const COLOR_PRIMARY = '#910016'
export const theme = createTheme({
	palette: {
		primary: {
			main: COLOR_PRIMARY
		}
	}
})
