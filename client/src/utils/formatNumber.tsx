export const formatNumber = (number?: number): string => {
	if (!number) return '-'
	return Math.floor(number).toLocaleString('cs-Cz').replaceAll('\xa0', ' ')
}
