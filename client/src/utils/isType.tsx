

export const isTypeString = (props: unknown): props is string => Object.prototype.hasOwnProperty.call(props, 'name')
