import { allPass, isEmpty, isNil, not } from 'ramda'

export const isNotNilOrEmpty = (
	value?: string | boolean | undefined | Record<string, string | number | boolean | undefined> | null
): boolean => not(allPass([isNil, isEmpty])(value))
