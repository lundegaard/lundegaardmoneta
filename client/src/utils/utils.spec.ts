import { formatNumber } from './formatNumber'
import { parseYearsAndMonthsByMonths } from './dates'

describe('Utils tests', () => {
	it('Number formatting', () => {
		expect(formatNumber(1000)).toEqual('1 000')
		expect(formatNumber(1000000)).toEqual('1 000 000')
		expect(formatNumber(1500000)).toEqual('1 500 000')
	})
	it('Dates formatting by months', () => {
		expect(parseYearsAndMonthsByMonths(24)).toEqual('2 roky')
		expect(parseYearsAndMonthsByMonths(12)).toEqual('1 roky')
		expect(parseYearsAndMonthsByMonths(25)).toEqual('2 roky a 1 měsíc')
		expect(parseYearsAndMonthsByMonths(26)).toEqual('2 roky a 2 měsíce')
		expect(parseYearsAndMonthsByMonths(28)).toEqual('2 roky a 4 měsíce')
		expect(parseYearsAndMonthsByMonths(29)).toEqual('2 roky a 5 měsíců')
		expect(parseYearsAndMonthsByMonths(30)).toEqual('2 roky a 6 měsíců')
		expect(parseYearsAndMonthsByMonths(35)).toEqual('2 roky a 11 měsíců')
	})
})
