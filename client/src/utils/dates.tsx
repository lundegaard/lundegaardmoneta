import { always, cond, equals, gte, isNil, or, lte, T } from 'ramda'

export const parseYearsAndMonthsByMonths = (months: number): string => {
	const years = Math.floor(months / 12)
	const diffMonths = months - years * 12
	const yearLabel = years <= 4 ? 'roky' : 'let'
	return or(isNil(months), equals(diffMonths, 0))
		? `${years} ${yearLabel}`
		: `${years} ${yearLabel} a ${diffMonths} ${translateMonths(diffMonths)}`
}

export const translateMonths = cond([
	[equals(1), always('měsíc')],
	[(val: number) => gte(val, 2) && lte(val, 4), always('měsíce')],
	[(val: number) => gte(val, 5), always('měsíců')],
	[T, always('')]
])
