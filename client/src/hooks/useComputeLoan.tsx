import { useDebounce } from './useDebounce'
import { ILoanFormValues } from '../components/organism/forms/LoanForm/schema'
import { useEffect } from 'react'
import { useFormikContext } from 'formik'
import { usePostLoanMutation } from './query/usePostLoanComputeMutation'
import { isEmpty } from 'ramda'
import { useAppDispatch } from '../redux/store'
import { updateFormState } from '../redux/loanForm/loanFormSlice'

export const useComputeLoan: () => void = () => {
	const { values, isValidating, isSubmitting, errors } = useFormikContext<ILoanFormValues>()
	const debounceCompute = useDebounce<ILoanFormValues>(values, 500)
	const [postForComputationValueMutation] = usePostLoanMutation()
	const dispatch = useAppDispatch()
	useEffect(() => {
		dispatch(updateFormState(values))
		if (debounceCompute && !isValidating && !isSubmitting && isEmpty(errors)) {
			postForComputationValueMutation({
				term: values.term,
				amount: values.loan,
				insurance: values.insurance
			})
		}
	}, [debounceCompute])
}
