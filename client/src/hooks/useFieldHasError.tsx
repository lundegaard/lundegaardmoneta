import { and, propOr } from 'ramda'
import { useField, useFormikContext } from 'formik'
import { isNotNilOrEmpty } from '../utils/ramdaExtensions'

export const useFieldHasError = (fieldName: string): [boolean, string] => {
	const [field, meta] = useField(fieldName)
	const { errors } = useFormikContext()
	return [and(isNotNilOrEmpty(meta.error), meta.touched), propOr('', field.name)(errors)]
}
