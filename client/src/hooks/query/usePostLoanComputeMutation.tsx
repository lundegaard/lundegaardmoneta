import { usePostLoanComputeMutation } from '../../api/loan/loadnComputationApi'

export const usePostLoanMutation = () => usePostLoanComputeMutation({ fixedCacheKey: 'loan-compute-mutation' })
