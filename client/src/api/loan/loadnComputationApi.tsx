import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { ILoanComputeRequest, ILoanComputeResponse } from './types'
import { API } from '../../constants/API'

export const loanApi = createApi({
	reducerPath: 'loanComputeApi',
	baseQuery: fetchBaseQuery({ baseUrl: API.server }),
	tagTypes: ['Loan'],
	endpoints: (builder) => ({
		postLoanCompute: builder.mutation<ILoanComputeResponse, ILoanComputeRequest>({
			query(data) {
				return {
					url: '/loan',
					method: 'POST',
					body: data
				}
			},
			transformResponse: (result: ILoanComputeResponse) => result
		})
	})
})
export const { usePostLoanComputeMutation } = loanApi
