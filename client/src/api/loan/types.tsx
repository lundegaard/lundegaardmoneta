export interface ILoanComputeRequest {
	amount: number
	term: number
	insurance: boolean
}

export interface ILoanComputeResponse {
	monthly: number
	rate: number
	overallAmount: number
	totalDifference: number
	insurance: number
}
