import React from 'react'
import { Box, Container, Divider, Grid, Typography } from '@mui/material'
import { LoanForm } from '../../components/organism/forms/LoanForm'
import { LoanFormResultCard } from '../../components/molecules/LoanFormResultCard'

export const LoanPage: React.FC = () => {
	return (
		<Container maxWidth={'lg'}>
			<Box display={'flex'} justifyContent={'center'} alignItems={'center'}>
				<Grid container spacing={6}>
					<Grid item md={8}>
						<Typography fontWeight={500} textAlign={'left'} variant={'h5'} mb={2}>
							Sjednejte si půjčku online bez poplatku
						</Typography>
						<Divider />
						<LoanForm />
					</Grid>
					<Grid item md={4}>
						<LoanFormResultCard />
					</Grid>
				</Grid>
			</Box>
		</Container>
	)
}
