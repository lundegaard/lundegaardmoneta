import { useAppSelector } from '../../redux/store'
import { getFormState } from '../../redux/loanForm/loanFormSlice'
import React from 'react'

export const LookupPage: React.FC = () => {
	const iLoanFormValues = useAppSelector(getFormState)
	return <div>{JSON.stringify(iLoanFormValues)}</div>
}
