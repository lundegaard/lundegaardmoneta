<img src="ReadmeImages/app.png">

### Spuštění aplikace 
* ```yarn run client-build```
* ```yarn run start-production```

Zde by měla být aplikace dostupná:
```http://localhost:5000```

## DEV Stack
* Podmínkou bylo využití Redux, zvolil jsem nejunámější nástroj pro správu stavu v Reduxu a to je Redux/Toolkit
* Ten nyní přišel s novým konceptem, který kopíruje technologie jako je React-Query, a tedy snažil jsem se využít těchto novinek a je tedy napojený RTK (Redux Toolkit Query)
* Ten jsem využil pro volání REST API s BE pomocí mutací a automatického ukládání stavu volání do Redux stavu, kde je snadné se dostav po sléze ke stavu mutace
* Toto by šlo řešit i asynchroními reducery, ašak jsem již zvyklí pracovat s React-Query, takže tato varianta mě přišla bližší
* Formulář
  * Využil jsem knihovnu Formik, díky největší znalosti, a jednoduché implmeentaci validací, a napojení Atom komponent na knihovnu
  * Proč jsem nevuyžil Redux/Forms ? 
    * Je všeobecně známo, že Redux-Forms není vhodný pro ukládání stavu formuláře. Ani já jsem se nerozhodl tuto knihovnu využít i když bylo požadavkem mít stavu formuláře v Redux stavu. Hlavní nevýhodou Redux-Forms je veliký Performance issue, kde při velkýc aplikacích velice zpomlauje celý chod aplikace.
  * Aktualozace stavu porbíha pomocí debounce/hooks
  * Samozřejmě by se dalo naslouchat na každém inputu onFocus, ale rozhodl jsem se pro tuto variantu, jelikož dává dobré možnosti při každé změně provolat REST API na výpočet nových hondot půjčky.
  * Debounce je nastaven na 500ms, a to hlavně kvůli Sliderům, které by bez Debounce/hooks volaly API zbytečně mockrát
  * Při deounce se provolá API a zároveň se aktuaizuje stav formuláře v Redux stavu
  * Validace na Formik jsou psány pomocí knihovny Yup, a jsou zde obsaženy všechyn validace, které byly vypsány v zadání
  * Po Submit formu pomocí tlačítka pokrařovat, je znovu uložen stav formuláře do Redux stavu, a uživatel jse přeměrován na obrazovku, kde se již pouze pomocí Redux selectoru, zobrazují data z Redux storu.

### Závěrem
Doufám že jsem obhájil a aspoň trochu vysvětlil použitý DEV stack na FE, a a jak jsem již zmiňoval, jsem spíše zastánce React-Query.  