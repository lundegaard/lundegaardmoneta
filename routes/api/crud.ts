import * as express from "express";
import {ILoanComputeRequest} from '../../types/loan/ILoanComputeRequest';
import {ILoanComputeResponse} from '../../types/loan/ILoanComputeResponse';
const LoanCalc = require('loan-calc');

const router = express.Router();

/**
 * @method POST
 * @access public
 * @endpoint /api/v1/loan
 **/
router.post<{}, ILoanComputeResponse, ILoanComputeRequest, {} >('/loan', (req , res) => {
    const requestBody = req.body
    const value = LoanCalc.paymentCalc({
        amount: requestBody.amount,
        rate: 6.7,
        termMonths: requestBody.term
    })
    const INSURANCE = 20000
    const totalAmountWithoutInsurance = requestBody.term * value
    const totalAmount = req.body.insurance ? totalAmountWithoutInsurance + INSURANCE : totalAmountWithoutInsurance
    const totalDifference = (requestBody.term * value) - requestBody.amount
    setTimeout(() => res.json({
        monthly: value,
        rate: 6.7,
        overallAmount: totalAmount,
        totalDifference: totalDifference,
        insurance: req.body.insurance ? INSURANCE : 0
    }), 1000)
});


module.exports = router;
