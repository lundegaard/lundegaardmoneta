export interface ILoanComputeResponse {
    monthly: number
    rate: number
    overallAmount: number,
    totalDifference: number,
    insurance: number
}