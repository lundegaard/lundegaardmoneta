export interface ILoanComputeRequest {
    amount: number
    term: number
    insurance: boolean
}
